// var http = require('http');
//
// var server = http.createServer(function(req, res){
//   console.log('request was made: ' + req.url);
//   res.writeHead(200, {'Content-Type': 'text/plain'});
//   res.end('hello world');
// });
//
// server.listen(3000, '127.0.0.1');
// console.log('listening to port 3000');

//git pull
//git status
//git add .
//git commit -m "commit message"
//git push origin master

// hit "npm start"!!!!!!!!!!!!!!!!!!!!!

//const cors = require('cors');
//hello
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 4000;
const Order = require('./order_methods.js');
const Account = require('./account_methods.js');
var http = require('http');
app.use(bodyParser.json());
var mysql = require('mysql');


var con = mysql.createConnection({
  host : 'easystirfry.cj3bihyckj3m.us-east-2.rds.amazonaws.com',
  user : 'root',
  password : 'BDapp2018',
  database : 'easystirfrydb'
});

con.connect(function(err) {
  if (err) throw err;
  console.log('connected to database');
});

function makeOrderObject(order) {
    const newOrder = new Order(order.id, order.time_in, order.protein, order.extra_protein,order.veggies,
      order.extra_veggies, order.grain, order.sauce, order.time_out, order.phone_number);
    return newOrder;
}

function makeAccountObject(account) {
  const newAccount = new Account(account.washu_id, account.password, account.name, account.saved_order, account.phonenumber);
  return newAccount;
}

//User interface routes below

//working
app.get('/WaitTime', function (req, res, next) {
  var sql = "SELECT * FROM main WHERE time_out = '0'";
  con.query(sql, function(err, result){
    if (err) throw err;

    const length = result.length;
    const time = length*2;
    const response = {
     // totalOrders: length,
     waitTime: time
   };
   res.json(response);
  });
});

//working
app.post('/SignIn', function (req, res, next) {
  console.log(req.body);
  var sql = `SELECT * FROM accounts
  WHERE washu_id = '${req.body.user}'
  AND password = '${req.body.pw}'`;

  con.query(sql, function(err, result) {
    if (err) throw err;
    if (result.length == 0) {
      const response = {
        errMsg: 'No good query',
        querySuccess: false
      };
      res.json(response);
    }
    else {
      const account = result[0];
      const x = makeAccountObject(account);
      const response = {
        washu_id: x.getId(),
        //pw: x.getPassword(),
        querySuccess: true
      };
      res.json(response);
    }
  });
});

//working
app.post('/PlaceOrder', function(req, res) {
  let response;
  if (req.body.saveorder == true) {
    console.log('order is saved');
    var sql3 = `SELECT * FROM accounts WHERE washu_id = '${req.body.id}'`;
    con.query(sql3, function(err, result) {
      if (err) throw err;

      const account = result[0];
      console.log('a: ');
      console.log(account);
      const z = makeAccountObject(account);
      var sql = `SELECT * FROM main WHERE id = '${z.getSavedOrderId()}'`;
      con.query(sql, function(err, result) {
        if (err) throw err;
        if (result.length == 0) {
          response = {
            errMsg : 'No saved order',
            querySuccess : false
          }
          res.json(response);
        }
        else {
          const order = result[0];
          console.log('o');
          //console.log(order);
          const x = makeOrderObject(order);
          x.resetOrder();
          console.log(x);
          x.sendOrder();
          response = {
            querySuccess : true
          }
          res.json(response);
        }
      });
    });
  }
  else {
    /*
    const ordervars = {
      protein: selectedprotein,
      extraprotein: selectedxprotein,
      veggies: s,
      extraveggies: extraveggies,
      grain: selectedgrain,
      sauce: selectedsauce,
      phonenumber: phonenumber,
      id: sessionStorage.getItem('wustlID'),
      saveorder: savedOrder
    };

    */
    console.log(req.body);
    const newOrder = new Order(null, Date.now(), req.body.protein, req.body.extraprotein, req.body.veggies,
    req.body.extraveggies, req.body.grain, req.body.sauce, 0, req.body.phonenumber);
    console.log('newOrder: ')
    console.log(newOrder);
    const sendOrder = newOrder.sendOrder();
    if (sendOrder == 'Query unsuccessful') {
      response = {
        querySuccess : false
      }
      res.json(response);
    }
    else {
      response = {
        querySuccess : true
      }
      res.json(response);
    }

  }
  // var sql = `SELECT * FROM main WHERE
  // protein = '${req.body.protein}' AND
  // extra_protein = '${req.body.extraprotein}' AND
  // veggies = '${req.body.veggies}' AND
  // extra_veggies = '${req.body.extraveggies}' AND
  // grain = '${req.body.grain}' AND
  // sauce = '${req.body.sauce}' AND
  // phone_number = '${req.body.phonenumber}'`;
  // con.query(sql, function(err, result) {
  //   if (err) throw err;
  //   const order = result[0];
  //   const x = makeOrderObject(order);
  //   var sql2 = `UPDATE accounts SET saved_order = ${x.getId()}
  //   WHERE washu_id = '${req.body.id}'`;
  //   con.query(sql2, function(err, result) {
  //     if (err) throw err;
  //     console.log('saved order updated')
  //   })
  // })
});

//working
app.post('/SignUp', function(req, res) {
  console.log('ping signup');
  console.log(req.body);
  const newAccount = new Account(req.body.id, req.body.pw, req.body.name,
  null, req.body.phonenumber);
  newAccount.sendAccount();
  newAccount.sendWelcomeSMS();
  res.json({
    id : req.body.id,
    querySuccess: true
  })
});

//working
app.get('/Profile/:id', function(req, res, next) {
  var sql = `SELECT * FROM accounts WHERE washu_id = '${req.params.id}'`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    if (result.length == 0) {
      console.log('no account');
      response = {
        querySuccess : false
      }
      res.json(response);
    }
    else {
      const account = result[0];
      const y = makeAccountObject(account);
      const id = y.getSavedOrderId();
      console.log(id);
      const n = result[0].phone_number;
      console.log(n);
      var sql2 = `SELECT * FROM main WHERE id = '${id}'`;
      con.query(sql2, function(err, result) {
        if (err) throw err;
        if (result.length == 0) {
          console.log('no saved order');
          response = {
            name : y.getName(),
            phonenumber : n,
            querySuccess : true
          }
          res.json(response);
        }
        else {
          const order = result[0]
          const x = makeOrderObject(order);
          const response = {
            protein: x.resetProtein(),
            extra_protein: x.resetExtraProtein(),
            veggies: x.getVeggies(),
            extra_veggies: x.resetExtraVeggies(),
            grain: x.resetGrain(),
            sauce: x.resetSauce(),
            phonenumber: x.getPhoneNumber(),
            name: y.getName(),
            querySuccess: true
          }
          res.json(response);
        }
      });
    }
  });
});

//working
app.post('/EditProfile', function(req, res) {
  var sql = `UPDATE accounts SET name = "${req.body.name}" WHERE washu_id = '${req.body.id}'`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    if (result.length == 0) {
      const response = {
        querySuccess : false
      }
      res.json(response);
    }
    else {
    console.log('name updated');
    res.json({
       querySuccess: true
      })
    }
    });
  });

//not caring about
app.post('/ResetPassword', function(req, res) {
  const w = Math.floor(Math.random()*10);
  const x = Math.floor(Math.random()*10);
  const y = Math.floor(Math.random()*10);
  const z = Math.floor(Math.random()*10);
  const code = w + '' + x + '' + y + '' + z;
  console.log(code);
  var sql = `SELECT * FROM accounts WHERE washu_id = ${req.body.id}`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    const a = result[0];
    const account = makeAccountObject(a);
    account.sendSMS(code);
    if (req.body.code = code) {
      var sql2 = `UPDATE accounts SET password = ${req.body.newpw}
      WHERE washu_id = '${req.body.id}'`;
      con.query(sql2, function(err, result) {
        if (err) throw err;
        console.log('password updated');
        const response = {
          code : code,
          passwordReset : true
        };
      });
    }
    else {
      const response = {
        code : code,
        passwordReset : false
      }
    }
    res.json(response);
  })
})

//working
app.post('/EditSavedOrder', async function(req, res, next) {
  console.log(req.body);
  const timeIn = Date.now();
  const timeOut = 1;
  const order = new Order(null, timeIn, req.body.protein, req.body.extraprotein, req.body.veggies,
  req.body.extraveggies, req.body.grain, req.body.sauce, timeOut, req.body.phonenumber);
  console.log(order);
  const r = await order.sendOrderAsync();
  console.log(r);
  var sql = `SELECT * FROM main WHERE id = ${r.insertId}`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    console.log(result);
    if (result.length == 0) {
      res.json({
        querySuccess : false
      })
    }
    else {
      const order1 = makeOrderObject(result[0]);
      // var sql5 = `UPDATE main SET time_out = '${timeOut}' WHERE id = '${order1.getId()}'`;
      // con.query(sql5, function(err, result) {
      //   if (err) throw err;
      //   console.log('time_out set to nonzero')
      // })
      var sql2 = `UPDATE accounts SET saved_order = ${order1.getId()}
      WHERE washu_id = '${req.body.id}'`;
      con.query(sql2, function(err, result) {
        if (err) throw err;
        console.log('Saved order updated');
      });
      const response = {
        querySuccess : true
      };
      res.json(response);
    }
  });
});

//not working
app.post('/AddRealOrder', function(req, res) {
  if (req.body.realorder == 'true') {
    const veggies = 'paper order: ' + Date.now();
    const newOrder = new Order(null, null, null, null,
    veggies, null, null, null, null, null);
    newOrder.sendOrder();
    var sql = `SELECT * FROM main WHERE veggies = '${veggies}'`;
    con.query(sql, function(err, result) {
      if (err) throw err;
      if (result.length == 0) {
        const response = {
          querySuccess : false
        }
        res.json(response);
      }
      else {
        const order = result[0];
        const x = makeOrderObject(order);
        const response = {
          querySuccess : true
        }
        res.json(response);
      }
    })
  }
  else {
    res.json({
      querySuccess : false
    })
  }
})

//Doing employee interface routes below

//working
app.get('/ActiveOrders', function(req, res, next) {
  var sql = "SELECT * FROM main WHERE time_out = '0'";
  con.query(sql, function(err, result) {
    if (err) throw err;
    var activeOrders = [];
    var i;
    for (i = 0; i < result.length; i++) {
      const order = makeOrderObject(result[i]);
      activeOrders[i] = order.getId();
    }
    const response = {
      activeorders: activeOrders,
      querySuccess: true
    };

    const badRes = {
      errMsg: 'no good query',
      querySuccess: false
    };

    res.json(response);
  });
});

//working
app.get('/ViewOrder/:id', function(req, res, next) {
  var sql = `SELECT * FROM main WHERE id = ${req.params.id}`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    if (result.length == 0) {
      const response = {
        querySuccess : false
      }
      res.json(response);
    }
    else {
      const order = result[0];
      const x = makeOrderObject(order);
      let response = {
        id: x.getId(),
        protein: x.resetProtein(),
        extra_protein: x.resetExtraProtein(),
        veggies: x.getVeggies(),
        extra_veggies: x.resetExtraVeggies(),
        grain: x.resetGrain(),
        sauce: x.resetSauce(),
        time_out: Date.now(),
        querySuccess: true
      };
      res.json(response);
    }
  });
});

//working
app.get('/OrderReady/:id', function (req, res, next) {
  var sql = `SELECT * FROM main WHERE id = '${req.params.id}'`;
  con.query(sql, function(err, result) {
    if (err) throw err;
    const order = result[0];
    const x = makeOrderObject(order);
    const response = {
      id: x.getId(),
      protein: x.getProtein(),
      extra_protein: x.getExtraProtein(),
      veggies: x.getVeggies(),
      extra_veggies: x.getExtraVeggies(),
      grain: x.getGrain(),
      sauce: x.getSauce(),
      time_out: Date.now(),
      querySuccess: true
    };
    x.sendSMS();
    var sql2 = `UPDATE main SET time_out = '${Date.now()}' WHERE id = '${req.params.id}'`;
    con.query(sql2, function(err, result){
      if (err) throw err;
      console.log('time_out updated');
    })

    const badRes = {
      errMsg: 'No good query',
      querySuccess: false
    };
    res.json(response);
  });
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/Website/HomePage.html');
});

app.get('/:filename', (req, res) => {
  res.sendFile(__dirname + '/Website/'+ req.params.filename);
});

app.get('/ping', (req, res) => {
  //res.header('Access-Control-Allow-Origin', '*');
  res.send('pong');
});

app.listen('4000', () => {
  console.log('server listening on port 4000');
});
