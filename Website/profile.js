if (sessionStorage.getItem('wustlID') == null || sessionStorage.getItem('wustlID') == "") {
  window.location.href = '/SignIn.html';
}
document.addEventListener('DOMContentLoaded', () => {
  console.log(sessionStorage.getItem('wustlID'));
  document.getElementById('wustlID').innerHTML = sessionStorage.getItem('wustlID'),
  fetch('/Profile/' + sessionStorage.getItem('wustlID'), {
    method: 'GET',
  }).then(res => res.json()).then(res => {
      if(res.querySuccess==false) {
        alert('bad res, please refresh page');
      }
      if(res.querySuccess==true) {
        document.getElementById('phonenumber').innerHTML = res.phonenumber,
        document.getElementById('topname').innerHTML = res.name,
        document.getElementById('protein').innerHTML = res.protein,
        document.getElementById('xprotein').innerHTML = res.extra_protein,
        document.getElementById('vegetables').innerHTML = res.veggies,
        document.getElementById('xvegetables').innerHTML = res.extra_veggies,
        document.getElementById('grain').innerHTML = res.grain,
        document.getElementById('sauce').innerHTML = res.sauce
      }
  });
});
