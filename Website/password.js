document.addEventListener('DOMContentLoaded', () => {
  if (sessionStorage.getItem('wustlID') == null) {
    window.location.href = '/SignIn.html';
  }); document.getElementById('resetPassword').addEventListener("click", (event) => {
    if (document.getElementById('confirmPassword').value != document.getElementById('newPassword').value) {
      alert('Passwords do not match');
    }
    const credentials = {
      id: window.SessionStorage('wustlID'),
      resetNumber: document.getElementById('resetNumber').value,
      newPassword: document.getElementById('newPassword').value,
      confirmPassword: document.getElementById('confirmPassword').value
    };
    fetch('/ResetPassword', {
      method: 'POST',
      body: JSON.stringify(credentials),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(res => res.json()).then(
      response => {
        console.log(response);
        if (res.passwordResetIsGood) { //still waiting on route to match this response
          window.location.href = '/Profile.html';
          alert('You have reset your password');
        } else {
          alert('Invalid Reset Credentials');
        }
      }
    ).catch(err => console.error(err));
  )
});
