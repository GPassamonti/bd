document.getElementById('submit').addEventListener('click', (event) => {
if (document.getElementById('wustlId').value == "" || document.getElementById('password').value=="" || document.getElementById('name').value =="" ||  document.getElementById('phonenumber').value=="") {
  alert("Invalid Sign-Up Credentials, please fill out each area.");
}
else {
  const credentials = {
    id: document.getElementById('wustlId').value,
    pw: document.getElementById('password').value,
    name: document.getElementById('name').value,
    phonenumber: document.getElementById('phonenumber').value
  };
  console.log(credentials);
  fetch('/SignUp', {
    method: 'POST',
    body: JSON.stringify(credentials),
    headers: {
      "Content-Type": "application/json"
    }
  }).then (res => res.json()).then( res => {
      sessionStorage.setItem('wustlID', res.id);
      console.log(res.signupsuccess)
      alert('Thank you for signing up!');
      window.location.href = '/HomePage.html';

  });
}
});
