document.addEventListener('DOMContentLoaded', () => {
fetch("/ViewOrder/" + sessionStorage.getItem('orderID'), {
  method: 'GET',
}).then(res => res.json()).then( res => {
    console.log(res);
    if(res.querySuccess==true) {
    document.getElementById('protein').innerHTML = res.protein;
    document.getElementById('xprotein').innerHTML = res.extra_protein;
    document.getElementById('vegetables').innerHTML = res.veggies;
    document.getElementById('xvegetables').innerHTML = res.extra_veggies;
    document.getElementById('grain').innerHTML = res.grain;
    document.getElementById('sauce').innerHTML = res.sauce;
  }
  else {
    alert('cannot load order details');
    }
  });
});
