if (sessionStorage.getItem('wustlID') == null || sessionStorage.getItem('wustlID') == "" ) {
  window.location.href = '/SignIn.html';
}
document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('signout').addEventListener('click', (event) => {
    sessionStorage.setItem('wustlID', "");
    console.log('logged out');
    alert('You have been succesfully logged out');
    window.location.href = '/SignIn.html';
  });
});
