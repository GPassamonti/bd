document.addEventListener('DOMContentLoaded', () => {
  //add divs for each current online order
  fetch('/ActiveOrders', {
    method: 'GET'
  }).then(res => res.json()).then(res => {
    console.log(res);
    const wrapper = document.getElementById('smallbox');

    res.activeorders.forEach(order => {

      const orderWrapper = document.createElement('div');
      orderWrapper.classList.add('order-wrapper');

      const innerWrapper = document.createElement('div');
      innerWrapper.classList.add('order');
      innerWrapper.classList.add('orderonline');

      innerWrapper.innerHTML = order;

      const remove = document.createElement('input');
      remove.classList.add('delete');
      remove.type = 'submit';
      remove.value = 'X';

      remove.addEventListener('click', (event) => {
        console.log(order);
        fetch('/OrderReady/' + order, {
          method: 'GET',
        }).then(
          res => res.json()
        ).then(res => {
          if (res.querySuccess == true) {
            innerWrapper.remove();
            orderWrapper.remove();
          }
          if (res.querySuccess == false) {
            alert('Error in server response, please refresh page');
          };
        });
      });

      const view = document.createElement('input');
      view.classList.add('view');
      view.type = 'submit';
      view.value = 'Details';

      view.addEventListener('click', (event) => {
        sessionStorage.setItem('orderID', order);
        //console.log(res.id);
        //sessionStorage.setItem('orderID', res.id);
        //console.log(sessionStorage.getItem('orderID'));
        window.location.href = '/WorkerOrder.html'
      });


      innerWrapper.appendChild(remove);

      innerWrapper.appendChild(view);

      orderWrapper.appendChild(innerWrapper);

      wrapper.appendChild(orderWrapper);

    });
  });

//   document.getElementById('addorder').addEventListener('click', (event) => {
//     const credentials = {
//       realorder: true
//     }
//     fetch('/AddRealOrder', {
//       method: 'POST',
//       body: JSON.stringify(credentials),
//       headers: {
//         "Content-Type": "application/json"
//       }
//     }).then(res => res.json()).then(
//       res => {
//
//         fetch('/ActiveOrders', {
//           method: 'GET'
//         }).then(res => res.json()).then(res => {
//
//           console.log(order.id);
//
//           const wrapper = document.getElementById('smallbox');
//
//           const orderWrapper = document.createElement('div');
//           orderWrapper.classList.add('order-wrapper');
//
//           const realorder = document.createElement('div');
//           realorder.classList.add('order');
//           realorder.classList.add('realorder');
//
//           const remove = document.createElement('input');
//           remove.classList.add('delete');
//           remove.type = 'submit';
//           remove.value = 'X';
//
//           realorder.innerHTML = 'Real Order:' + order.id;
//
//           realorder.appendChild(remove);
//
//           orderWrapper.appendChild(realorder);
//
//           wrapper.appendChild(orderWrapper);
//
//           remove.addEventListener('click', (event) => {
//             orderWrapper.remove();
//           });
//         });
//       });
//   });
 });
