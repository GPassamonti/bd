if (sessionStorage.getItem('wustlID') == null || sessionStorage.getItem('wustlID') == "") {
  window.location.href = '/SignIn.html';
}
document.addEventListener('DOMContentLoaded', () => { //signin if not already
  console.log(sessionStorage.getItem('wustlID'));
  document.getElementById('stirfrysubmit').addEventListener("click", (event) => {
      if (document.getElementById("phonenumber").value == '') {
        alert('Please enter a valid phone number');
      }
      else {
        let savedOrder = false;
        if (($('#savedorder').is(':checked'))) { //use saved order specifics
          savedOrder = true;
        };

        let s = ''; //all vegetables
        if (($('#broccoli').is(':checked'))) {
          s = s + ' ' + 'broccoli';
        }
        if (($('#carrots').is(':checked'))) {
          s = s + ' ' + 'carrots';
        }
        if (($('#snowpeas').is(':checked'))) {
          s = s + ' ' + 'snowpeas';
        }
        if (($('#edamame').is(':checked'))) {
          s = s + ' ' + 'edamame';
        }
        if (($('#bamboo').is(':checked'))) {
          s = s + ' ' + 'bamboo';
        }
        if (($('#bellpepper').is(':checked'))) {
          s = s + ' ' + 'bellpepper';
        }
        if (($('#cucumber').is(':checked'))) {
          s = s + ' ' + 'cucumber';
        }
        if (($('#corn').is(':checked'))) {
          s = s + ' ' + 'corn';
        }
        if (($('#onions').is(':checked'))) {
          s = s + ' ' + 'onions';
        }
        if (($('#mushrooms').is(':checked'))) {
          s = s + ' ' + 'mushrooms';
        }
        if (($('#spinach').is(':checked'))) {
          s = s + ' ' + 'spinach';
        }
        if (($('#ginger').is(':checked'))) {
          s = s + ' ' + 'ginger';
        }
        if (($('#garlic').is(':checked'))) {
          s = s + ' ' + 'garlic';
        }
        let extraveggies = false;
        if (($('#extraveggies').is(':checked'))) { //use saved order specifics
          extraveggies = true;
        }

        var selectedprotein = $('#protein').val();

        var selectedxprotein = $('#extraprotein').val();

        var selectedgrain = $('#grain').val();

        var selectedsauce = $('#sauce').val();

        var phonenumber = document.getElementById('phonenumber').value;

        const ordervars = {
          protein: selectedprotein,
          extraprotein: selectedxprotein,
          veggies: s,
          extraveggies: extraveggies,
          grain: selectedgrain,
          sauce: selectedsauce,
          phonenumber: phonenumber,
          id: sessionStorage.getItem('wustlID'),
          saveorder: savedOrder
        };

        console.log(ordervars);

        fetch('/PlaceOrder', {
            method: 'POST',
            body: JSON.stringify(ordervars),
            headers: {
              "Content-Type": "application/json"
            }
        }).then(res => res.json()).then(res => {
        if (res.querySuccess == true) {
          alert('Your order has been succesfully placed');
          window.location.href = '/ConfirmOrder.html';
        } else {
          alert('Please refresh the page and order again');
        }
      })
    }
  });
});
