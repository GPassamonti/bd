if(sessionStorage.getItem('wustlID') == null || sessionStorage.getItem('wustlID')== "") {
  document.getElementById("signin").addEventListener("click", (event) => {
  console.log('hello world');
  const credentials = {
    user: document.getElementById("wustlID").value,
    pw: document.getElementById("password").value
  }
  fetch('/SignIn', { //routes from stephens post and get routes
    method: 'POST',
    body: JSON.stringify(credentials),
    headers: {
      "Content-Type": "application/json"
    }
  }).then
  (res => res.json()).then(
    res => {
    if (res.querySuccess==true) {
      console.log(res.washu_id);
      sessionStorage.setItem('wustlID', res.washu_id );
      console.log(sessionStorage.getItem('wustlID'));
      window.location.href = '/HomePage.html';
    }
    else if (res.querySuccess == false) {
      alert("Invalid Login");
    }
    else{
      alert(res.errMsg);
    }
  });
});
}
else {
  alert('You are already signed in');
  window.location.href = "/HomePage.html";
}
