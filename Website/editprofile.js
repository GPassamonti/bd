document.addEventListener('DOMContentLoaded', () => { //signin if not already
if(sessionStorage.getItem('wustlID') == null) {
  window.location.href = '/SignIn.html';
};
document.getElementById('save').addEventListener('click', (event) => {
  const profile = {
    name: document.getElementById('editname').value,
    id: sessionStorage.getItem('wustlID')
  }
  fetch('/EditProfile', {
    method: 'POST',
    body: JSON.stringify(profile),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(res => res.json()).then( res => {
    if (res.querySuccess==true) {
      window.location.href = '/Profile.html';
    }
    else {
      alert('Please resubmit form');
    }
  });
});
});
