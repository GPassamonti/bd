//main class for order methods, assigning UI inputs to values to be inputted to DB
// changed function to class
function Order(id, time_in, protein, extra_protein, veggies, extra_veggies, grain, sauce, time_out, phone_number) {
  this.id = id;
  this.in = time_in;
  this.p = protein;
  this.ep = extra_protein;
  this.v = veggies;
  this.ev = extra_veggies;
  this.g = grain;
  this.s = sauce;
  this.out = time_out;
  this.n = phone_number;
}

Order.prototype.getId = function() {
  return this.id;
}

Order.prototype.getTimeIn = function() {
  return this.in;
}

Order.prototype.getProtein = function() {
  return this.p;
}

Order.prototype.getExtraProtein = function() {
  return this.ep;
}

Order.prototype.getVeggies = function() {
  return this.v;
}

Order.prototype.getExtraVeggies = function() {
  return this.ev;
}

Order.prototype.getGrain = function() {
  return this.g;
}

Order.prototype.getSauce = function() {
  return this.s;
}

Order.prototype.getTimeOut = function() {
  return this.out;
}

Order.prototype.getPhoneNumber = function() {
  return this.n;
}

Order.prototype.setTimeIn = function() {
  return Date.now();
}

Order.prototype.setProtein = function() {
  if (this.p == 'Beef') {
    return 1;
  }
  else if (this.p == 'Chicken') {
    return 2;
  }
  else if (this.p == 'Shrimp') {
    return 3;
  }
  else if (this.p == 'Pork') {
    return 4;
  }
  else if (this.p == 'Tofu') {
    return 5;
  }
  else if (this.p == 'Tempeh') {
    return 6;
  }
  else {
    return 0;
  }
}

Order.prototype.resetProtein = function() {
  if (this.p == 1) {
    return 'Beef'
  }
  else if (this.p == 2) {
    return 'Chicken'
  }
  else if (this.p == 3) {
    return 'Shrimp'
  }
  else if (this.p == 4) {
    return 'Pork'
  }
  else if (this.p == 5) {
    return 'Tofu'
  }
  else if (this.p == 6) {
    return 'Tempeh'
  }
  else {
    return 'None'
  }
}

Order.prototype.setExtraProtein = function() {
  if (this.ep == 'Beef') {
    return 1;
  }
  else if (this.ep == 'Chicken') {
    return 2;
  }
  else if (this.ep == 'Shrimp') {
    return 3;
  }
  else if (this.ep == 'Pork') {
    return 4;
  }
  else if (this.ep == 'Tofu') {
    return 5;
  }
  else if (this.ep == 'Tempeh') {
    return 6;
  }
  else {
    return 0;
  }
}

Order.prototype.resetExtraProtein = function() {
  if (this.ep == 1) {
    return 'Beef'
  }
  else if (this.ep == 2) {
    return 'Chicken'
  }
  else if (this.ep == 3) {
    return 'Shrimp'
  }
  else if (this.ep == 4) {
    return 'Pork'
  }
  else if (this.ep == 5) {
    return 'Tofu'
  }
  else if (this.ep == 6) {
    return 'Tempeh'
  }
  else {
    return 'None'
  }
}

Order.prototype.setVeggies = function() {
  return this.v;
}

Order.prototype.setExtraVeggies = function() {
  if (this.ev == true) {
    return 1;
  }
  else {
    return 0;
  }
}

Order.prototype.resetExtraVeggies = function() {
  if (this.ev == 1) {
    return true;
  }
  else {
    return false;
  }
}

Order.prototype.setGrain = function() {
  if (this.g == 'Lo Mein Noodles'){
    return 1;
  }
  else if (this.g == 'Brown Rice'){
    return 2;
  }
  else if (this.g == 'White Rice'){
    return 3;
  }
  else if (this.g == 'Rice Noodles'){
    return 4;
  }
  else {
    return 0;
  }
}

Order.prototype.resetGrain = function() {
  if (this.g == 1) {
    return 'Lo Mein Noodles';
  }
  else if (this.g == 2) {
    return 'Brown Rice';
  }
  else if (this.g == 3) {
    return 'White Rice';
  }
  else if (this.g == 4) {
    return 'Rice Noodles';
  }
  else {
    return 'None';
  }
}

Order.prototype.setSauce = function() {
  if (this.s == 'Chinese Lemon'){
    return 1;
  }
  else if (this.s == 'Mongolian Black Bean'){
    return 2;
  }
  else if (this.s == 'Spicy Hoisin'){
    return 3;
  }
  else if (this.s == 'Sweet and Sour Pineapple'){
    return 4;
  }
  else if (this.s == 'Teriyaki BBQ'){
    return 5;
  }
  else {
    return 0;
  }
}

Order.prototype.resetSauce = function() {
  if (this.s == 1) {
    return 'Chinese Lemon';
  }
  else if (this.s == 2) {
    return 'Mongolian Black Bean';
  }
  else if (this.s == 3) {
    return 'Spicy Hoisin';
  }
  else if (this.s == 4) {
    return 'Sweet and Sour Pineapple';
  }
  else if (this.s == 5) {
    return 'Teriyaki BBQ';
  }
  else {
    return 'None';
  }
}

Order.prototype.resetOrder = function() {
  this.p = this.resetProtein();
  this.ep = this.resetExtraProtein();
  this.ev = this.resetExtraVeggies();
  this.g = this.resetGrain();
  this.s = this.resetSauce();
}
const utils = require('util');

Order.prototype.sendOrder = function() {
  var mysql = require('mysql');

  var con = mysql.createConnection({
    host : 'easystirfry.cj3bihyckj3m.us-east-2.rds.amazonaws.com',
    user : 'root',
    password : 'BDapp2018',
    database : 'easystirfrydb'
  });

  con.connect(function(err) {
    if (err) throw err;
    console.log('connected to database');
  });

  var sql = `INSERT INTO main (time_in, protein, extra_protein, veggies,
  extra_veggies, grain, sauce, phone_number)
  VALUES (${this.setTimeIn()}, ${this.setProtein()}, ${this.setExtraProtein()},
  '${this.setVeggies()}', ${this.setExtraVeggies()}, ${this.setGrain()},
  ${this.setSauce()}, ${this.getPhoneNumber()})`;
  console.log(sql);
  con.query(sql, function(err, result){
    if (err) throw err;
    if (result.length == 0) {
      return 'Query unsuccessful';
    }
    console.log('1 entry inserted')
  });
}

Order.prototype.sendOrderAsync = async function() {
  var mysql = require('mysql');

  var con = mysql.createConnection({
    host : 'easystirfry.cj3bihyckj3m.us-east-2.rds.amazonaws.com',
    user : 'root',
    password : 'BDapp2018',
    database : 'easystirfrydb'
  });

  const q = utils.promisify(con.query).bind(con);

  con.connect(function(err) {
    if (err) throw err;
    console.log('connected to database');
  });

  var sql = `INSERT INTO main (time_in, protein, extra_protein, veggies,
  extra_veggies, grain, sauce, phone_number)
  VALUES (${this.setTimeIn()}, ${this.setProtein()}, ${this.setExtraProtein()},
  '${this.setVeggies()}', ${this.setExtraVeggies()}, ${this.setGrain()},
  ${this.setSauce()}, ${this.getPhoneNumber()})`;
  console.log(sql);
  return q(sql);
  /* con.query(sql, function(err, result){
    if (err) throw err;
    console.log('1 entry inserted')
    return 1;
  });
  */
}

Order.prototype.sendSMS = function() {
  if (this.n == null) {
    console.log('no phone number');
  }
  else {
  const accountSid = 'AC47c1342c107c0f26f5054026bb0358a8';
  const authToken = '5e34be66ca748b69486fd77c646212ab';
  const client = require('twilio')(accountSid, authToken);

  client.messages
    .create({
       body: 'Your BD stir fry order is ready! Enjoy!',
       from: '+14013365377',
       to: '+' + 1 + this.n
     })
    .then(message => console.log(message.sid))
    .done();
  console.log('SMS sent to ' + '+1' + this.n);
  }
}

// Order.prototype.pullOrder() = function(var x) {
//   var mysql = require('mysql');
//
//   var con = mysql.createConnection({
//     host : 'easystirfry.cj3bihyckj3m.us-east-2.rds.amazonaws.com',
//     user : 'root',
//     password : 'BDapp2018',
//     database : 'easystirfrydb'
//   });
//
//   con.connect(function(err) {
//     if (err) throw err;
//     console.log('connected to database');
//   });
//
//   var sql = `SELECT * FROM main WHERE id = ` + x;
//   con.query(sql, function(err, result){
//     if (err) throw err;
//     console.log('1 entry inserted')
//   });
// }

module.exports = Order;
