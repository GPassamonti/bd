//class for account object using accounts DB

function Account(washu_id, password, name, saved_order, phone_number) {
  this.id = washu_id;
  this.pw = password;
  this.name = name;
  this.saved = saved_order;
  this.n = phone_number;
}

Account.prototype.getId = function() {
  return this.id;
}

Account.prototype.getPassword = function() {
  return this.pw;
}

Account.prototype.getName = function() {
  return this.name;
}

Account.prototype.getSavedOrderId = function() {
  return this.saved;
}

Account.prototype.getPhoneNumber = function() {
  return this.n;
}


//successful
Account.prototype.sendAccount = function() {
  var mysql = require('mysql');
  var con = mysql.createConnection({
    host : 'easystirfry.cj3bihyckj3m.us-east-2.rds.amazonaws.com',
    user : 'root',
    password : 'BDapp2018',
    database : 'easystirfrydb'
  });

  con.connect(function(err) {
    if (err) throw err;
    console.log('connected to database');
  });

  var sql = `INSERT INTO accounts (washu_id, password, name, saved_order, phone_number)
  VALUES (${this.getId()}, '${this.getPassword()}',
  '${this.getName()}', ${this.getSavedOrderId()}, ${this.getPhoneNumber()})`;
  con.query(sql, function(err, result){
    if (err) throw err;
    console.log('1 entry inserted')
  });
}

Account.prototype.sendSMS = function(code) {
  const accountSid = 'AC47c1342c107c0f26f5054026bb0358a8';
  const authToken = '5e34be66ca748b69486fd77c646212ab';
  const client = require('twilio')(accountSid, authToken);

  client.messages
    .create({
       body: 'Your password reset code for Easy Stir Fry is ' + code,
       from: '+14013365377',
       to: '+' + 1 + this.n
     })
    .then(message => console.log(message.sid))
    .done();
  console.log('SMS sent to ' + '+1' + this.n)
}

Account.prototype.sendWelcomeSMS = function(code) {
  const accountSid = 'AC47c1342c107c0f26f5054026bb0358a8';
  const authToken = '5e34be66ca748b69486fd77c646212ab';
  const client = require('twilio')(accountSid, authToken);

  client.messages
    .create({
       body: 'Thank you for signing up for Easy Stir Fry!',
       from: '+14013365377',
       to: '+' + 1 + this.n
     })
    .then(message => console.log(message.sid))
    .done();
  console.log('Welcome SMS sent to ' + '+1' + this.n)
}


module.exports = Account;
